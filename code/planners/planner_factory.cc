#include <assert.h>
#include <math.h>
#include <set>
#include <stdlib.h>

#include "planner_factory.h"

#include "greedy.h"
#include "labeled_ssipp.h"
#include "lrtdp.h"
#include "random.h"
#include "ssipp.h"
#include "vi.h"

#include "../ext/mgpt/global.h"
#include "../ext/mgpt/hash.h"
#include "../simulators/simulator.h"
#include "../ext/mgpt/states.h"

Planner* createPlanner(SSPIface const& ssp, std::string const& name) {
  if (!strcasecmp(name.c_str(), "random")) {
    return new PlannerRandom(ssp);
  }
  else if (!strcasecmp(name.c_str(), "vi")) {
    return new PlannerVI(ssp, *gpt::hstack.top(), gpt::epsilon);
  }
  else if (!strcasecmp(name.c_str(), "lrtdp"))  {
    return new PlannerLRTDP(ssp, *gpt::hstack.top(), gpt::epsilon,
                                MAX_TRACE_SIZE, false);
  }
  else if (!strncasecmp(name.c_str(), "labeledssipp:", 12)) {
    return new PlannerLabeledSSiPP(ssp, *gpt::hstack.top(), gpt::epsilon,
                                      name.substr(12));
  }
  else if (!strncasecmp(name.c_str(), "ssipp", 5)) {
     return new PlannerSSiPP(ssp, *gpt::hstack.top(), gpt::epsilon,
                                name.substr(5));
  }
  std::cout << "[Warning] No SSP planner named '" << name << "'" << std::endl;
  return nullptr;
}
