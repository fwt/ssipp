#include <iostream>
#include <sstream>
#include <string.h>

#include "heuristic_factory.h"
#include "constant_value.h"
#include "h_add.h"
#include "h_max.h"
#include "lm_cut.h"

#include "../utils/die.h"
#include "../ext/mgpt/global.h"

/*******************************************************************************
 *
 * heuristic_t
 *
 ******************************************************************************/
// static
extern "C" char* strtok_r(char*, const char*, char**) throw();

void createHeuristics(SSPIface const& ssp,
    std::string const& heuristic, std::stack<heuristic_t*>& hstack)
{
  heuristic_t *heur;
  char *strstack = strdup(heuristic.c_str());
  char *lptr, *ptr = strtok_r(strstack, "| ", &lptr);
  while (ptr != NULL) {
    /*
     * heuristic_t, i.e., SSPIface based heuristics
     */
    if (!strcasecmp(ptr, "simpleZero")) {
      heur = new ZeroHeuristic();
    }
    else if (!strcasecmp(ptr, "smartZero")) {
      heur = new SmartZeroHeuristic(ssp);
    }
    else {
      /*
       * FactoredHeuristic or descendants
       */
      // TODO(fwt): refactor the code to avoid this
      problem_t const& problem = *gpt::problem;

      if (!strcasecmp(ptr, "lm-cut")) {
        heur = new LMCutHeuristic(problem);
      }
      else if (!strcasecmp(ptr, "h-add")) {
        heur = new HAddAllOutcomesDet(problem, ACTION_COST);
      }
      else if (!strcasecmp(ptr, "h-max")) {
        heur = new HMaxAllOutcomesDet(problem, ACTION_COST);
      }
      else {
        std::cerr << "ERROR! undefined heuristic `" << ptr << "'" << std::endl;
        exit(-1);
      }
    }  // case in which the heuristic is a FactoredHeuristic
    hstack.push(heur);
    ptr = strtok_r(NULL, "| ", &lptr);
  }
  free(strstack);
}

// static
void destroyHeuristics(std::stack<heuristic_t*>& hstack) {
  while( !hstack.empty() ){
    delete hstack.top();
    hstack.pop();
  }
}
