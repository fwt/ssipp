#ifndef HEURISTICS_FACTORY_H
#define HEURISTICS_FACTORY_H

#include <iostream>
#include <stack>

#include "heuristic_iface.h"
#include "../ssps/ssp_iface.h"


void createHeuristics(SSPIface const& ssp, std::string const& heuristic,
                              std::stack<heuristic_t*>& hstack);
void destroyHeuristics(std::stack<heuristic_t*>& hstack);


#endif  // HEURISTICS_FACTORY_H
