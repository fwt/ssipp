#include <cerrno>
#include <ctime>
#include <fstream>
#include <iostream>
#include <signal.h>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <unistd.h>

#include "../../utils/exceptions.h"
#include "global.h"
#include "../../utils/utils.h"



// SignalManager: Registring callback function
SignalManager::SignalManager() {
  // SIGURS1: dump the current log
  signal(SIGUSR1, SignalManager::sigUsr1Handler);
  // SIGTERM: dump the log and then quit
  signal(SIGTERM, SignalManager::sigTermHandler);
}

// static
void SignalManager::sigUsr1Handler(int signum) {
  std::cout << "Caught signal " << signum << std::endl;
  if (signum != 10) {
    exit(signum);
  }
//  std::cout << "Saving log..." << std::endl;
}

// static
void SignalManager::sigTermHandler(int signum) {
  std::cout << "Caught signal " << signum << std::endl;
  if (signum != 15) {
    exit(signum);
  }
  std::cout << "Saving the log and quitting..." << std::endl;
  exit(signum);
}



namespace gpt
{
  uint64_t parsing_cpu_time = 0;
#ifdef USE_CACHE_PROB_OP_ADDS_ATOM
  size_t total_saved_prob_op_adds_atom_calls = 0;
  size_t total_prob_op_adds_atom_calls = 0;
#endif
  bool randomize_actionsT_order = true;
  double given_value_for_vStar_s0 = 0;
  uint64_t max_cpu_sys_time_usec = 0;
  uint64_t max_rss_kb = 0;
  Deadline* _deadline_ = 0;
  StopCriterion stopCriterion = NONE;
  SignalManager signalManager;
  std::string ff_path = "./ff_mod";
  std::string lama_path = "./lama";
  std::string legend_file = "";
  std::set<std::string> debug_signals = parseDebugSignals();
  double ignore_effects_with_prob_less_than = 0.0;
  std::shared_ptr<PlannerFFReplan> followable_ffreplan;
  std::string followable_ffreplan_param = "";
  bool external_ff_ignore_forall_w_prob_effects = false;
  bool suppress_round_info = false;
  bool default_hp = true;
  std::string algorithm = "lrtdp";
  bool domain_analysis = false;
  Rational dead_end_value = Rational(500); //UINT_MAX;
  unsigned bound = 0;
  unsigned cutoff = 0;
  double epsilon = 0.0001;
  bool hash_all = true;
  std::string heuristic = "smartZero";
  size_t initial_hash_size = 204800;
  unsigned max_database_size = 32;
  bool noise = false;
  double noise_level = 0;
  unsigned seed = 0;
  int simulations = 0;
  unsigned verbosity = 0;
  unsigned warning_level = 0;
  double heuristic_weight = 1;
  size_t xtra = 0;
  std::stack<heuristic_t*> hstack;
  bool print_turn_details = false;
  std::string execution_simulator = "local";
  uint32_t total_execution_rounds = 30;
  int max_time_in_secs = 0;
  uint64_t start_time = 0;
  problem_t const* problem = NULL;
  Simulator const* simulator = NULL;
  bool show_applied_policy = false;
  bool show_computed_policy = false;
  uint32_t max_turn = 10000;
  Rational cost_shift = Rational(0);

  /*
   * The default is to ignore state cost and consider only action costs. To:
   *  - also consider state cost (i.e., action and state), use -C full
   *  - ignore all the costs (both action and state), use -C ignore
   */
  bool use_action_cost = true;
  bool use_state_cost = false;
  bool normalize_action_cost = false;

  std::string tmp_dir = "/tmp/";
};


#if MEM_DEBUG

  void *
operator new( size_t size )
{
  void *result = malloc( size );
  fprintf( stderr, "new %p %d\n", result, size );
  return( result );
}

  void *
operator new[]( size_t size )
{
  void *result = malloc( size );
  fprintf( stderr, "new[] %p %d\n", result, size );
  return( result );
}

  void
operator delete( void *ptr )
{
  if( ptr )
  {
    fprintf( stderr, "del %p\n", ptr );
    free( ptr );
  }
}

  void
operator delete[]( void *ptr )
{
  if( ptr )
  {
    fprintf( stderr, "del[] %p\n", ptr );
    free( ptr &);
  }
}

#endif // MEM_DEBUG


bool readArguments(int& argc, char**& argv, std::vector<char*>& remaining_args)
{
  if (argc == 1) goto usage;
  ++argv;
  --argc;
  while (argc > 0 && argv[0][0] == '-') {
    if (strlen(argv[0]) == 2) {
      // Single letter argument
      switch (argv[0][1]) {
        case 'a':
          gpt::hash_all = (gpt::hash_all?false:true);
          ++argv;
          --argc;
          break;
        case 'A':
          gpt::show_applied_policy = true;
          argv += 1;
          argc -= 1;
          break;
        case 'b':
          gpt::tmp_dir = argv[1];
          argv += 2;
          argc -= 2;
          break;
        case 'c':
          gpt::cutoff = atoi( argv[1] );
          argv += 2;
          argc -= 2;
          break;
        case 'C':
          if (!strncasecmp(argv[1], "ignore", 6)) {
            gpt::use_action_cost = false;
            gpt::use_state_cost = false;
            gpt::normalize_action_cost = false;
          } else if (!strncasecmp(argv[1], "action", 6)) {
            gpt::use_action_cost = true;
            gpt::use_state_cost = false;
            gpt::normalize_action_cost = false;
            if (!strcasecmp(argv[1], "action:normalize"))
              gpt::normalize_action_cost = true;
            else if (strlen(argv[1]) > 6) {
              std::cout << "Option '-C " << argv[1]
                << "' not recognized" << std::endl;
              exit(-1);
            }
          } else if (!strncasecmp(argv[1], "full", 4)) {
            gpt::use_action_cost = true;
            gpt::use_state_cost = true;
            gpt::normalize_action_cost = false;
            if (!strcasecmp(argv[1], "full:normalize"))
              gpt::normalize_action_cost = true;
            else if (strlen(argv[1]) > 4) {
              std::cout << "Option '-C " << argv[1]
                << "' not recognized" << std::endl;
              exit(-1);
            }
          } else {
            std::cout << "The option '-C " << argv[1]
              << "' was not recognized..."
              << std::endl;
            exit(-1);
          }
          argv += 2;
          argc -= 2;
          break;
        case 'd':
          gpt::dead_end_value = (size_t) gpt::heuristic_weight * atoi(argv[1]);
          argv += 2;
          argc -= 2;
          break;
        case 'D':
          gpt::max_time_in_secs = atoi(argv[1]);

          if (gpt::max_time_in_secs > 0) {
            struct rlimit rl = { (rlim_t) gpt::max_time_in_secs,
                                 (rlim_t) gpt::max_time_in_secs };
            if (setrlimit(RLIMIT_CPU, &rl)) {
              std::cout << "ERROR trying to set the CPU time limit\n";
              std::cerr << "ERROR trying to set the CPU time limit\n";
            } else
              std::cout << "CPU-TIME LIMIT: " << gpt::max_time_in_secs
                << " secs" << std::endl;
          }
          argv += 2;
          argc -= 2;
          break;
        case 'e':
          gpt::epsilon = atof( argv[1] );
          argv += 2;
          argc -= 2;
          break;
        case 'h':
          gpt::default_hp = false;
          //          !strncmp(gpt::heuristic,argv[1],9) && (strlen(argv[1])==19) && !strcmp(&gpt::heuristic[10],&argv[1][10]);
          gpt::heuristic = argv[1];
          argv += 2;
          argc -= 2;
          break;
        case 'i':
          gpt::initial_hash_size = atoi( argv[1] );
          argv += 2;
          argc -= 2;
          break;
        case 'm':
          gpt::max_database_size = atoi( argv[1] );
          argv += 2;
          argc -= 2;
          break;
        case 'M':
          gpt::max_turn = (uint32_t) atoi(argv[1]);
          argv += 2;
          argc -= 2;
          break;
        case 'p':
          gpt::default_hp = false;
          gpt::algorithm = argv[1];
          argv += 2;
          argc -= 2;
          break;
        case 'P':
          gpt::show_computed_policy = true;
          argv += 1;
          argc -= 1;
          break;
        case 'r':
          gpt::seed = atoi(argv[1]);
          argv += 2;
          argc -= 2;
          break;
        case 'R':
          gpt::total_execution_rounds = (uint32_t) atoi(argv[1]);
          DIE(gpt::stopCriterion == NONE,
              "Another stop criterion was specified before", -1);
          gpt::stopCriterion = NUM_ROUNDS;
          argv += 2;
          argc -= 2;
          break;
        case 's':
          gpt::simulations = atoi( argv[1] );
          argv += 2;
          argc -= 2;
          break;
        case 't':
          gpt::print_turn_details = true;
          argv += 1;
          argc -= 1;
          break;
        case 'v':
          gpt::verbosity = atoi( argv[1] );
          argv += 2;
          argc -= 2;
          break;
        case 'w':
          gpt::heuristic_weight = atof(argv[1]);
          gpt::dead_end_value = gpt::dead_end_value * (size_t) gpt::heuristic_weight;
          argv += 2;
          argc -= 2;
          break;
        case 'x':
          gpt::xtra = atoi( argv[1] );
          argv += 2;
          argc -= 2;
          break;
        case 'X':
          gpt::execution_simulator = argv[1];
          argv += 2;
          argc -= 2;
          break;
        case 'z':
          gpt::domain_analysis = (gpt::domain_analysis?false:true);
          ++argv;
          --argc;
          break;
        default:
          std::cout << "Flag not recognized: " << argv[0] << std::endl;
          goto usage;
      }  // end switch
    }  // end if strlen(argv[0]) == 2
    else if (strlen(argv[0]) > 2 && argv[0][1] == '-') {
      // Long option, i.e., --something
      if (!strcasecmp(*argv, "--legend")) {
        gpt::legend_file = std::string(argv[1]);
        argv += 2;
        argc -= 2;
      } else if (!strcasecmp(*argv, "--ff_path")) {
        gpt::ff_path = std::string(argv[1]);
        argv += 2;
        argc -= 2;
      } else if (!strcasecmp(*argv, "--lama_path")) {
        gpt::lama_path = std::string(argv[1]);
        argv += 2;
        argc -= 2;
      } else if (!strncasecmp(*argv, "--follow_ff:", 12)) {
        gpt::followable_ffreplan_param = std::string(argv[0]);
        argv += 1;
        argc -= 1;
      } else if (!strcasecmp(*argv, "--suppress_round_info")) {
        gpt::suppress_round_info = true;
        argv += 1;
        argc -= 1;
      } else if (!strcasecmp(*argv, "--ignore_effects_with_prob_less_than")) {
        gpt::ignore_effects_with_prob_less_than = atof(argv[1]);
        argv += 2;
        argc -= 2;
      } else if (!strcasecmp(*argv, "--conv_s0")) {
        DIE(gpt::stopCriterion == NONE,
            "Another stop criterion was specified before", -1);
        gpt::stopCriterion = CONV_S0;
        argv += 1;
        argc -= 1;
      } else if (!strcasecmp(*argv, "--conv_cost")) {
        DIE(gpt::stopCriterion == NONE,
            "Another stop criterion was specified before", -1);
        gpt::given_value_for_vStar_s0 = atof(argv[1]);
        gpt::stopCriterion = CONV_COST;
        argv += 2;
        argc -= 2;
      } else if (!strcasecmp(*argv, "--max_rss_kb")) {
        gpt::max_rss_kb = atoi(argv[1]);
        argv += 2;
        argc -= 2;
      } else if (!strcasecmp(*argv, "--max_cpu_time_sec")) {
        gpt::max_cpu_sys_time_usec = 1000000 * (uint64_t) atoi(argv[1]);
        argv += 2;
        argc -= 2;
      } else if (!strcasecmp(*argv, "--no_rand_action_ordering")) {
        gpt::randomize_actionsT_order = false;
        argv += 1;
        argc -= 1;
      } else {
        std::cout << "Flag not recognized: " << argv[0] << std::endl;
        goto usage;
      }
    }  // end if strlen(argv[0]) > 2
    else {
      goto usage;
    }
  }

  if (argc == 0 || argc > 2) {
usage:
    printUsageGlobal(std::cout);
    if (argc > 0) {
      std::cerr << "\n\nERROR! Parameter '" << argv[0] << "' not recognized!"
                << " Didn't try the parameters after that one too...\n" << std::endl;
    }
    else {
      std::cerr << "\n\nERROR! Not enough parameters" << std::endl;
    }
    exit(7);
  }
  else {
    for (int i = 0; i < argc; i++) {
      remaining_args.push_back(argv[i]);
    }
    return true;
  }
}

void printUsageGlobal(std::ostream& os) {
  printBanner(os);
  printUsageLocal(os);
  os << std::endl << std::endl
     << "==================================================================================="
     << std::endl
     << "GLOBAL Options:" << std::endl << std::endl
     << "  -d <dead-end-value>     (default = " << gpt::dead_end_value << ", see options bellow)" << std::endl
     << "  -e <epsilon>            (default = " << gpt::epsilon << ")" << std::endl
     << "  -h <heuristic-stack>    (default = \"smartZero\")" << std::endl
     << "  -M <max_turn>           (max number of actions applicable in each round. Default = "
     << gpt::max_turn << ")" << std::endl
     << "  -p <planner>            (default = lrtdp)" << std::endl
     << "  -r <random-seed>        (default = random)" << std::endl
     << "  -R <rounds>             (default = 30)" << std::endl
     << "  -t                      (Turn on the output of each turn detail)" << std::endl
     << "  --max_rss_kb <K>        (Maximum resident memory allowed in KB. Default = unlimited)" << std::endl
     << "  --max_cpu_time_sec <T>  (Maximum CPU time allowed in seconds. Default = unlimited)" << std::endl
     << "  --suppress_round_info" << std::endl
     << "  <planner>         := random | vi | lrtdp | ssipp | labeledssipp"
     << std::endl
     << "  <heuristic-stack> := <heuristic-stack> '|' <heuristic> | <heuristic>" << std::endl
     << "  <heuristic>       := simpleZero | smartZero | h-max | h-add | lm-cut " << std::endl
     << std::endl
     << "A stop criterion must be provided, that is, one of the following flags must be passed: " << std::endl
     << "  -R <number of round>" << std::endl
     << "  --conv_cost <cost> <limit>  wait until |V(s0) - cost| <= epsilon" << std::endl
     << "  --conv_s0 <limit>           wait the epsilon-consistent of V to V*" << std::endl
     << "where <limit> is either (or both) --max_rss_kb <K> or --max_cpu_time_sec <T>"
     << std::endl << std::endl;
}

std::string current_file;
extern int yyparse();
extern FILE* yyin;

bool readPDDLFile(char const* name) {
  yyin = fopen(name, "r");
  if (yyin == NULL) {
    std::cout << "parser:" << name << ": " << strerror(errno) << std::endl;
    return (false);
  }
  else {
    current_file = name;
    bool success;
    try {
      success = (yyparse () == 0);
    }
    catch (Exception exception) {
      fclose(yyin);
      std::cout << exception << std::endl;
      return false;
    }
    fclose(yyin);
    return success;
  }
}
